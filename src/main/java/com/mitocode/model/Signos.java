package com.mitocode.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "signos")
public class Signos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSignos;

	@ManyToOne
	@JoinColumn(name = "id_paciente", nullable = false)
	private Paciente paciente;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;
	
	@NotNull(message = "La temperatura no puede ser nula")
	@DecimalMin(value = "1", message = "La temperatura debe ser mayor o igual a {value}")
	@DecimalMax(value = "100", message = "La temperatura debe ser menor o igual a {value}")
	@Column(name = "temperatura")
	private Double temperatura;
	
	@NotNull(message = "El pulso no puede ser nulo")
	@Min(value = 1, message = "El pulso debe ser mayor o igual a {value}")
	@Max(value = 300, message = "El pulso debe ser menor o igual a {value}")
	@Column(name = "pulso")
	private Integer pulso;
	
	@NotNull(message = "El ritmo respiratorio no puede ser nulo")
	@Min(value = 1, message = "El pulso debe ser mayor o igual a {value}")
	@Max(value = 100, message = "El pulso debe ser menor o igual a {value}")
	@Column(name = "ritmo_respiratorio")
	private Integer ritmoRespiratorio;

	public int getIdSignos() {
		return idSignos;
	}

	public void setIdSignos(int idSignos) {
		this.idSignos = idSignos;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public Integer getPulso() {
		return pulso;
	}

	public void setPulso(Integer pulso) {
		this.pulso = pulso;
	}

	public Integer getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}

	public void setRitmoRespiratorio(Integer ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}
	
	
}
