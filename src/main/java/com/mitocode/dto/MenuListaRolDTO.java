package com.mitocode.dto;

import java.util.List;

import com.mitocode.model.Menu;
import com.mitocode.model.Rol;

public class MenuListaRolDTO {
	private Menu menu;
	private List<Rol> roles;
	
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public List<Rol> getRoles() {
		return roles;
	}
	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
}
